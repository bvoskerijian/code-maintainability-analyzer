﻿# Code Maintainability Analyzer

## Overview

This is a project for [CSE-681 Software
Modeling](http://ecs.syr.edu/faculty/fawcett/handouts/CSE681-OnLine/Lectures/CSE681-OnLine.htm),
a graduate computer science course at Syracuse Univerity. The project
description that follows has been taken from the course website.

## Provided Project Description

### Purpose

This project develops a program for quality analysis of C# code to measure the
maintainablity of functions and classes. Different code metrics will be analized
to generate a final maintainablity index. The Code Maintainability Analyzer
computes the following metrics: size (LOC) and cyclomatic complexity for each
function, and cohesion1 and coupling2 for each class in each file of a specified
file set.

- Size: The tool computes size in terms of the number of lines of code.

- Cyclomatic Complexity: This refers to the number of independent paths in a
  function.

- Cohesion: We are going to use one of the simplest structural cohesion metric
  proposed by Chidamber & Kemerer in 1991 known as lack of cohesion in methods
  (LCOM1). LCOM1 measures the cohesion in terms of the number of pairs of
  functions that do not share any data members.

- Coupling: we will use this metric to measure dependency between classes. A
  class A depends on class B if A inherits, aggregates, composes, or uses class
  B. We are going to use the number of classes that one class depends on as the
  coupling measure for that class.

Finally the Code Maintainability Analyzer computes a maintainibility index for
each class using these four metrics as:

    MI = a * Size + b * CyclomaticComplexity + c * Cohesion + d * Coupling

The purpose of this code analysis is to find functions and classes that may need
attention because of their their quality metrics. A secondary purpose is to help
potential developers or managers quickly understand the structure of a set of
code.

### Requirements

Your Code Maintainability Analyzer (CMA) Project:

- Shall be implemented in C#, using the .Net Framework. Use of a Graphical User
  Interface is optional, but not recommended, and may be implemented in either
  WinForms or Windows Presentation Foundation (WPF).

- Shall use Visual Studio, Community Edition available at no cost.

- Shall accept a specification of files using file patterns and a path from the
  command line.

- Shall provide output information that lists class names, overall
  maintainability index, cohesion and coupling values, function names, function
  sizes, and cyclomatic complexities for each class and function in each file,
  and a summary for all the files in the specified set.

- Shall provide an interface to list output based on a single metric value.

- Shall provide means to update coefficients used to compute the maintainability
  index.

- Shall support redirecting the output to a file.

- Your submission shall supply a compile.bat and run.bat file that compiles and
  runs your project without the user needing to specify any information, e.g.,
  you are providing default values for path and file set. Please make those be
  the relative path to your project and the files that make up your project.

## Implementation

### Assumptions

A number of simplifying assumptions were made to ease the implementation of the
CMA. Some were provided by the professor (👨‍🏫) while others were synthesized
by the student during development (👨‍🎓).

- 👨‍🏫 There is no need to parser methods that have been auto-generated from
  Properties (get/set)

- 👨‍🏫 Cyclomatic complexity may be calculated using the provided parser logic,
  which counts the number of nested scopes in each element.

- 👨‍🏫 The cohesion metric, LCOM1, may be defined as the number of unique
  method pairs in a class which do not access any common member variables.

- 👨‍🎓 Each semi-expression may contain only one class dependency. The
  inherited parser is designed to support semi-expressions which match only one
  rule, one time. Attempts to change this behavior were not successful.

- 👨‍🎓 The CMA parser does not handle tuples ([new in C#
  7.0](https://blogs.msdn.microsoft.com/dotnet/2017/03/09/new-features-in-c-7-0/)).
  The use of tuples can cause the CMA to mistake member variables for methods
  due to the unique use of parentheses in the absence of a function.

### Usage

    Code Maintainability Analyzer

    Usage:
      .\Parser.exe .                      # Parse all files in this directory
      .\Parser.exe "sub/directory"        # Parse all files in subdirectory
      .\Parser.exe . -methods -v -d       # Show methods, variables, and dependencies
      .\Parser.exe . --o="out.txt"        # Show methods, variables, and dependencies
      .\Parser.exe . -p="P*.cs"           # Only searches for files with "cs." extension
      .\Parser.exe . -w="0, .3, .3, .3"   # Ignore LoC metric in MI calculation

    Options:
      -h --help                Display help
      -p --pattern=<pattern>   File pattern with wildcard (*) [default: *.cs]
      -o --output=<file>       Output file to write results to
      -d --dependencies        Show class dependencies
      -m --methods             Show class methods
      -v --variables           Show class variables
      -w --weights=<f,f,f,f>   Override weights to calculate MI (loc,cyc,coh,cou) [default: 1]

### Sample Ouput

#### Default

    Class                         LoC       Complexity     Cohesion  Coupling  MI
    ================================================================================

    Average                       47        8              1         0         56

    Processor                     122       18             1         0         141
    PopStack                      47        10             1         0         58
    PushStack                     37        5              1         0         43
    PrintFunction                 18        4              3         0         25
    Print                         11        3              1         0         15

#### With methods

    Class                         LoC       Complexity     Cohesion  Coupling  MI        
    ================================================================================

    Average                       47        8              1         0         56

    Processor                     122       18             1         0         141
      M:ParseFiles                119       17                                 
      M:Select                    7         1                                  
      No dependencies
    PopStack                      47        10             1         0         58
      M:PopStack                  3         1                                  
      M:doAction                  40        8                                  
      No dependencies
    PushStack                     37        5              1         0         43
      M:PushStack                 3         1                                  
      M:doAction                  30        3                                  
      No dependencies
    PrintFunction                 18        4              3         0         25
      M:PrintFunction             3         1                                  
      M:display                   7         1                                  
      M:doAction                  3         1                                  
      No dependencies
    Print                         11        3              1         0         15
      M:Print                     3         1                                  
      M:doAction                  4         1                                  
      No dependencies