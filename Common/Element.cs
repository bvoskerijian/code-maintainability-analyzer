﻿using System.Collections.Generic;

namespace CodeAnalyzer.Common
{
  public class Element
  {
    public Element()
    {
      Dependencies = new List<Element>();
      Members = new List<string>();
    }
    public string Type { get; set; }
    public string Name { get; set; }
    public int LinesOfCode { get; set; }
    public string FunctionClass { get; set; }
    public int CyclomaticComplexity { get; set; }
    public List<Element> Dependencies { get; set; }
    public List<string> Members { get; set; }
    public int Cohesion { get; set; }
    public float MaintainabilityIndex { get; set; }
  }
}