﻿using System.Collections.Generic;
using CodeAnalyzer.Common;
using Common;

namespace Parser.Metrics
{
  public class MaintainabilityIndex
  {
    public static MaintainabilityIndexOptions Options;

    public static float Calculate(List<Element> elements, Element element)
    {
      if (Options == null)
      {
        Options = new MaintainabilityIndexOptions();
      }

      return element.LinesOfCode * Options.LinesOfCodeWeight
        + element.CyclomaticComplexity * Options.ComplexityWeight
        + element.Cohesion * Options.CohesionWeight
        + element.Dependencies.Count * Options.CouplingWeight;
    }
  }
}
